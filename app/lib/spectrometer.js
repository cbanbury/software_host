export default class Spectrometer {
    /**
    * Perform initialisation of spectrometer
    */
    constructor() {
        // setup config variables
        this.serial_number;
        this.wavelength_calibration_coefficients = {
            0: null,
            1: null,
            2: null,
            3: null // coefficients for 0-4th order corrections
        };
        this.stray_light_constant;
        this.non_linearity_correction_coefficient = {
            0: null,
            1: null,
            2: null,
            3: null,
            4: null,
            5: null,
            6: null,
            7: null // coefficients for 0-7th order corrections
        };
        this.non_linearity_correction_polynomial_order;
        this.grating = {
            filter_wavelength: null,
            slit_size: null
        };

        // TODO: set default trigger mode
        // TODO: wait for CCD to cool
    }

    /**
    * Record new spectra using currently defined config
    */
    recordSpectra() {
        // TODO: match Request Spectra from USB spec
    }

    get CCDTemperature() {
    }
}
