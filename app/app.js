var server = require('http').createServer();
var io = require('socket.io')(server);
import Spectrometer from './lib/spectrometer';

io.on('connection', function(socket) {
    console.info('New client connection');
    // spectrometer events
    var spectrometer;
    socket.on('spectrometer:init', () => {spectrometer = new Spectrometer();});
    socket.on('spectrometer:recordSpectra', spectrometer.recordSpectra());
});

server.listen(3000, function(err) {
    if (err) {
        console.error('Unable to start server', {error: err});
        return;
    }
    console.info('ramanPi host started.')
});
