# ramanPi_software_host
This repository defines a server that acts as a common interface layer between user commands and physical hardware of the ramanPi.

A set of common functions are exposed that leaves the user free to connect via local hardware controls (touch controller/USB) or a web interface via a socket connection.

![](overview.png)

## Setup
The software_host is a simple node.js server using [socketio](https://socket.io/) to fire events between server and client.

To run locally:

1. Install node/npm: https://nodejs.org/en/download/

1. Install dependencies
```
npm install
```
1. Run server
```
npm start
```
